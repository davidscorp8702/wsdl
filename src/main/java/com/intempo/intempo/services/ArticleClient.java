package com.intempo.intempo.services;

import com.intempo.intempo.GetArticleRequest;
import com.intempo.intempo.GetArticleResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class ArticleClient extends WebServiceGatewaySupport {

    public GetArticleResponse getArticle(int id){
        GetArticleRequest getArticleRequest = new GetArticleRequest();
        getArticleRequest.setId(id);
        return (GetArticleResponse) getWebServiceTemplate().marshalSendAndReceive(getArticleRequest);
    }
}
