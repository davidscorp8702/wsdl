package com.intempo.intempo;

import com.intempo.intempo.config.SoapClientConfig;
import com.intempo.intempo.services.ArticleClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class IntempoApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntempoApplication.class, args);
		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(SoapClientConfig.class);
		ArticleClient articleClient = annotationConfigApplicationContext.getBean(ArticleClient.class);
		System.out.println(articleClient.getArticle(2).getArticle().getName());
	}

}
